package com.powerplantlab.dibstags

class DibsTagLib {

    static namespace = "dibs"


    def paymentwindow = { attrs, body ->

        out <<
        """
        <form name="${attrs.name}" method="POST" id="${attrs.id}" action="${grailsApplication.config.dibs.payment.window.url}" autocomplete="off">
            <input type="hidden" name="orderId" value="${attrs.orderId}">
            <input type="hidden" name="merchant" value="${grailsApplication.config.dibs.merchant}">
            <input type="hidden" name="amount" value="${attrs.amount}">
            <input type="hidden" name="currency" value="${attrs.currency}">
            <input type="hidden" name="language" value="${attrs.language}">
            <input type="hidden" name="acceptReturnUrl" value="${grailsApplication.config.dibs.acceptURL}">
            <input type="hidden" name="cancelreturnurl" value="${grailsApplication.config.dibs.cancelURL}">
            <input type="hidden" name="callbackUrl" value="${grailsApplication.config.dibs.callbackURL}">
            <input type="hidden" name="test" value="${!grailsApplication.config.dibs.test ?: 1 }">
            <input type="hidden" name="capturenow" value="${!grailsApplication.config.dibs.capturenow ?: 1}">
        """
        if(grailsApplication.config.dibs.checksumEnabled) {
            // Add all fields used above in ASCII order
            def hash = HashCalculator.calculateHash("${grailsApplication.config.dibs.mac.key}", "acceptReturnUrl=${grailsApplication.config.dibs.acceptURL}&amount=${attrs.amount}&callbackUrl=${grailsApplication.config.dibs.callbackURL}&cancelreturnurl=${grailsApplication.config.dibs.cancelURL}&capturenow=${!grailsApplication.config.dibs.capturenow ?: 1}&currency=${attrs.currency}&language=${attrs.language}&merchant=${grailsApplication.config.dibs.merchant}&orderId=${attrs.orderId}&test=${grailsApplication.config.dib.test}&")
            out <<  """    <input type="hidden" name="MAC" value="${hash}">"""
        }
        out <<
        """
        </form>
        """
    }
    def auth = { attrs, body ->

        out <<
        """
        <form name="${attrs.name}" method="POST" id="${attrs.id}" action="${grailsApplication.config.dibs.authURL}" autocomplete="off">
            <input type="hidden" name="transact" value="${attrs.transact}">
            <input type="hidden" name="merchant" value="${grailsApplication.config.dibs.merchant}">
            <input type="hidden" name="amount" value="${attrs.amount}">
            <input type="hidden" name="currency" value="${attrs.currency}">
            <input type="hidden" name="cardno" value="${attrs.cardno}">
            <input type="hidden" name="cvc" value="${attrs.cvc}">
            <input type="hidden" name="expmon" value="${attrs.expmon}">
            <input type="hidden" name="expyear" value="${attrs.expyear}">
            <input type="hidden" name="orderId" value="${attrs.orderId}">
            <input type="hidden" name="test" value="${grailsApplication.config.dibs.test ? "yes": "no"}">
            <input type="hidden" name="uniqueoid" value="yes">
            <input type="hidden" name="cardtype" value="${grailsApplication.config.dibs.cardtypes}">
            <input type="hidden" name="textreply" value="${grailsApplication.config.dibs.textreply}">
            <input type="hidden" name="capturenow" value="${grailsApplication.config.dibs.capturenow}">
        """
        if(grailsApplication.config.dibs.checksumEnabled) {
            out <<  """    <input type="hidden" name="md5key" value="${MD5Calculator.calculateMD5(grailsApplication.config.dibs.key1, grailsApplication.config.dibs.key2, grailsApplication.config.dibs.merchant, attrs.orderId, attrs.currency, attrs.amount)}">"""
        }
        out <<
        """
        </form>
        """
    }

    def _3DSecure = { attrs, body ->

        out <<
        """
        <form name="${attrs.name}" method="POST" id="${attrs.id}" action="${grailsApplication.config.dibs._3DSecureURL}" autocomplete="off">
            <input type="hidden" name="merchant" value="${grailsApplication.config.dibs.merchant}">
            <input type="hidden" name="accepturl" value="${grailsApplication.config.dibs.acceptURL}">
            <input type="hidden" name="cancelurl" value="${grailsApplication.config.dibs.cancelURL}">
            <input type="hidden" name="callbackurl" value="${grailsApplication.config.dibs.callbackURL}">
            <input type="hidden" name="amount" value="${attrs.amount}">
            <input type="hidden" name="currency" value="${attrs.currency}">
            <input type="hidden" name="cardno" value="${attrs.cardno}">
            <input type="hidden" name="cvc" value="${attrs.cvc}">
            <input type="hidden" name="expmon" value="${attrs.expmon}">
            <input type="hidden" name="expyear" value="${attrs.expyear}">
            <input type="hidden" name="orderId" value="${attrs.orderId}">
            <input type="hidden" name="test" value="${grailsApplication.config.dibs.test ? "yes": "no"}">
            <input type="hidden" name="uniqueoid" value="yes">
            <input type="hidden" name="cardtype" value="${grailsApplication.config.dibs.cardtypes}">
            <input type="hidden" name="textreply" value="${grailsApplication.config.dibs.textreply}">
            <input type="hidden" name="capturenow" value="${grailsApplication.config.dibs.capturenow}">
        """
        if(grailsApplication.config.dibs.checksumEnabled) {
            out <<  """    <input type="hidden" name="md5key" value="${MD5Calculator.calculateMD5(grailsApplication.config.dibs.key1, grailsApplication.config.dibs.key2, grailsApplication.config.dibs.merchant, attrs.orderId, attrs.currency, attrs.amount)}">"""
        }
        out <<
        """
        </form>
        """
    }
}
