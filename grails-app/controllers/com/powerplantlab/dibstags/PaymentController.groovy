package com.powerplantlab.dibstags

class PaymentController {

    def acceptHandlerService
    def cancelHandlerService
    def callbackHandlerService

    def index() {
        log.debug("payment index")
    }

    def accept() {
        log.debug("Handling accept")
        assert acceptHandlerService : "You need to set an acceptHandler"
        def orderParams = new OrderParams(params: params)
        acceptHandlerService.accept(orderParams)
    }

    def cancel() {
        log.debug("Handling cancel")
        assert cancelHandlerService : "You need to set an cancelHandler"
        def orderParams = new OrderParams(params: params)
        cancelHandlerService.cancel(orderParams)
    }

    def callback() {
        log.debug("Handling callback")
        assert callbackHandlerService : "You need to set an callbackHandler"
        def orderParams = new OrderParams(params: params)
        callbackHandlerService.callback(orderParams)
    }
}
