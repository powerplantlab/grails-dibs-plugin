package com.powerplantlab.dibstags

import groovy.transform.ToString
import groovy.transform.EqualsAndHashCode
import groovy.transform.TupleConstructor

/**
 * Order value class
 * @author Sigmund Lundgren
 * @since 2012-07-17 16:24
 */
@ToString @EqualsAndHashCode
class OrderValue {
    /**
     *    Dibs transaction ID
    */
    def transact
    /**
     *    Dibs status ACCEPTED/DECLINED
    */
    String status
    /**
     * Orderid as set by the application
     */
    def orderid
    /**
     * Order date
     */
    Date orderDate
}
