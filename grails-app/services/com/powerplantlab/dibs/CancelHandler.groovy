package com.powerplantlab.dibs
/**
 * @author Sigmund Lundgren
 * @since 2012-07-17 17:17
 */
interface CancelHandler {

    def cancel(order)
}