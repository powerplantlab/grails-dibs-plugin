package com.powerplantlab.dibs
/**
 * @author Sigmund Lundgren
 * @since 2012-07-17 17:16
 */
interface AcceptHandler {
    def accept(order)
}
