// configuration for plugin testing - will not be included in the plugin zip

dibs = {
    payment.window.url = "https://sat1.dibspayment.com/dibspaymentwindow/entrypoint"
    authURL = "https://payment.architrade.com/cgi-ssl/auth.cgi"
    _3DSecureURL = "https://payment.architrade.com/cgi-ssl/3dsecure.cgi"
    acceptURL = "https://localhost:8080/grails-dibs-plugin/payment/accept"
    cancelURL = "https://localhost:8080/grails-dibs-plugin/payment/cancel"
    callbackURL = "https://localhost:8080/grails-dibs-plugin/payment/callback"
    merchant = "123456"
    cardtypes = "AMEX, MC, VISA"
    textreply = "yes"
    capturenow = true
    key1 = "123456"
    key2 = "123456"
    checksumEnabled = true
    mac.key = "68756262616275626261676f79616461"
    test = 1
}

log4j = {
    // Example of changing the log pattern for the default console
    // appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',  //  controllers
           'org.codehaus.groovy.grails.web.pages', //  GSP
           'org.codehaus.groovy.grails.web.sitemesh', //  layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping', // URL mapping
           'org.codehaus.groovy.grails.commons', // core / classloading
           'org.codehaus.groovy.grails.plugins', // plugins
           'org.codehaus.groovy.grails.orm.hibernate', // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    warn   'org.mortbay.log'

    debug   'com.powerplantlab',
            'com.subdvd'
}

grails.views.default.codec="none" // none, html, base64
grails.views.gsp.encoding="UTF-8"
