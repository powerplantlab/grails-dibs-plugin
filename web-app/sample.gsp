<%@ page contentType="text/html;charset=UTF-8" %>
<g:javascript src="google-code-prettify/prettify.js"/>
<link rel="stylesheet" href="${resource(dir:'js/google-code-prettify',file:'prettify.css')}" />
<html>
  <head><title>DIBS sample</title></head>
  <body onload="prettyPrint()">

    <div>
        <h2>Auth Sample</h2>
        <pre class="prettyprint">
            <g:encodeAs codec="HTML"><dibs:auth id="auth" amount="200" currency="978" cardno="4020051000000000" cvc="684" expmon="6" expyear="24" orderId="99999"/></g:encodeAs>
        </pre>
        <dibs:auth id="auth" amount="200" currency="978" cardno="4020051000000000" cvc="684" expmon="6" expyear="24" orderId="99999"/>
    </div>

    <div>
      <h2>3DSecure Sample</h2>
      <pre class="prettyprint">
          <g:encodeAs codec="HTML"><dibs:_3DSecure id="auth" amount="200" currency="978" cardno="4020051000000000" cvc="684" expmon="6" expyear="24" orderId="99999"/></g:encodeAs>
      </pre>
      <dibs:auth id="_3DSecure" amount="200" currency="978" cardno="4020051000000000" cvc="684" expmon="6" expyear="24" orderId="99999"/>
    </div>

  </body>
</html>