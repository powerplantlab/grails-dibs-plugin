package com.powerplantlab.dibstags

/**
 * @author Sigmund Lundgren
 * @since 2012-04-20 12:08
 */
class MD5CalculatorTest extends GroovyTestCase {

    void testCalculateMd5() {
        assert "ac32dd38222632b17cece3a93f3cc505" == MD5Calculator.calculateMD5("1234", "5678", "Merchant", "1000", "978", "100")
    }
}
