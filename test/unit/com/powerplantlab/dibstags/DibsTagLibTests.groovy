package com.powerplantlab.dibstags



import grails.test.mixin.*

import com.powerplantlab.dibstags.DibsTagLib

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(DibsTagLib)
class DibsTagLibTests {

    void testSomething() {
        fail "Implement me"
    }
}
