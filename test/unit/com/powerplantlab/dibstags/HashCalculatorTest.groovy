package com.powerplantlab.dibstags

import org.junit.Test
/**
 * @author Sigmund Lundgren
 * @since 2012-08-28 13:15
 */
public class HashCalculatorTest {

    @Test
    public void testCalculateHash() throws Exception {
        assert "191996aaf41eeacf48baa37adfdfe294dce32f80cdea7955e4097dee463f2719" == HashCalculator.calculateHash("68756262616275626261676f79616461", "acceptReturnUrl=https://accept.url/&amount=100&currency=SEK&merchant=1234567&orderId=1234567890&test=1") as String
        assert "675d129dd8434ca7c4a9f7f39c5d1e2cf8c1fea4d6c62b5287ef5e3ed2782bfd" == HashCalculator.calculateHash("68756262616275626261676f79616461", "acceptReturnUrl=http://localhost:8080/subdvdg/payment/accept&amount=200&callbackUrl=http://localhost:8080/subdvdg/payment/callback&cancelreturnurl=http://localhost:8080/subdvdg/payment/cancel&capturenow=1&language=null&merchant=4220918&orderId=TEST-123456789&test=1") as String
        assert "a2fdbf4a9d7e134b0e6f89c3499e56315015e0a5815b1ac4c2abfaff0a924b5f" == HashCalculator.calculateHash("68756262616275626261676f79616461", "acceptReturnUrl=http://localhost:8080/subdvdg/payment/accept&amount=200&callbackUrl=http://localhost:8080/subdvdg/payment/callback&cancelreturnurl=http://localhost:8080/subdvdg/payment/cancel&capturenow=1&currency=SEK&language=sv_SE&merchant=4220918&orderId=TEST-123456789&test=1") as String
        assert "c3ba791659daf6765c2a5894fd4188166400288dc3652f8c0a06c257016797ae" == HashCalculator.calculateHash("2b7c6f593a6a485545422d47612d404744475b23365f3a37282c706d38766c406e2473674c5b704f23415d5f2965544d485a4e24714151306b78447d536e615a", "acceptReturnUrl=http://localhost:8080/subdvdg/payment/accept&amount=274&callbackUrl=http://localhost:8080/subdvdg/payment/callback&cancelreturnurl=http://localhost:8080/subdvdg/payment/cancel&capturenow=1&currency=SEK&language=sv_SE&merchant=4220918&orderId=223&test=1") as String
    }
}
