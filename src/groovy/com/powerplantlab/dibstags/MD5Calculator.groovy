package com.powerplantlab.dibstags

/**
 * DIBS md5 calculator
 * MD5(key2 + MD5(key1 + "merchant=<merchant>&orderid=<orderid>&currency= <currency>&amount= <amount>"))
 * @author Sigmund Lundgren
 * @since 2012-04-20 12:06
 */
class MD5Calculator {

    def static calculateMD5(key1, key2, merchant, orderId, currency, amount) {
        MD5Hash.md5(key2+MD5Hash.md5("${key1}merchant=${merchant}&orderid=${orderId}&currency=${currency}&amount=${amount}"))
    }
}
