package com.powerplantlab.dibstags

import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
/**
 * @author Sigmund Lundgren
 * @since 2012-08-28 13:13
 */
class HashCalculator {

    def static ALGORITHM = "HmacSHA256"

    def static calculateHash(keyHex, str) {
        def keyspec = new SecretKeySpec(keyHex.decodeHex(), ALGORITHM)

        def mac = Mac.getInstance(ALGORITHM)
        mac.init(keyspec)

        // Encode the string into bytes using utf-8 and digest it
        byte[] utf8 = str.getBytes("UTF8")
        byte[] digest = mac.doFinal(utf8)
        return digest.encodeHex()
    }
}
