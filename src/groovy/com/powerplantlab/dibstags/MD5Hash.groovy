package com.powerplantlab.dibstags

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
/**
 * @author Sigmund Lundgren
 * @since 2012-04-20 12:20
 */
public class MD5Hash {

    public static String md5(String what) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(what.getBytes(), 0, what.length());
            return new BigInteger(1, messageDigest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
