import org.slf4j.Logger
import org.slf4j.LoggerFactory

class DibsPluginGrailsPlugin {
    private Logger log = LoggerFactory.getLogger('grails.plugin.dibs.DibsPlugin')
    // the plugin version
    def version = "0.1"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.0 > *"
    // the other plugins this plugin depends on
    def dependsOn = [:]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp",
            "web-app/index.gsp",
            "web-app/sample.gsp"
    ]


    def title = "Grails Dibs Plugin" // Headline display name of the plugin
    def author = "Sigmund Lundgren"
    def authorEmail = "sigmund.lundgren@gmail.com"
    def description = '''\
DIBS payment provider integration for grails.
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/grails-dibs-plugin"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
    def organization = [name: "powerplantlab", url: "http://powerplantlab.com/"]

    // Any additional developers beyond the author specified above.
    def developers = [ [ name: "Sigmund Lundgren", email: "sigmund.lundgren@gmail.com" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://github.com/sigmundlundgren/grails-dibs-plugin/" ]

    def doWithWebDescriptor = { xml ->
        // TODO Implement additions to web.xml (optional), this event occurs before
    }

    def doWithSpring = {
        // TODO Implement runtime spring config (optional)
    }

    def doWithDynamicMethods = { ctx ->
        // TODO Implement registering dynamic methods to classes (optional)
    }

    def doWithApplicationContext = { applicationContext ->
        // TODO Implement post initialization spring config (optional)
    }

    def onChange = { event ->
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    def onConfigChange = { event ->
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    def onShutdown = { event ->
        // TODO Implement code that is executed when the application shuts down (optional)
    }

}
